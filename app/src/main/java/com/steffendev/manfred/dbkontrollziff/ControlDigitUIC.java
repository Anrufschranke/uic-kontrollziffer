/*
 * # Copyright (C) 2019 Manfred Steffen
 * # This file is part of UIC ControlDigit <https://gitlab.com/Anrufschranke/uic-kontrollziffer>.
 *  #
 * # UIC ControlDigit  is free software: you can redistribute it and/or modify
 * # it under the terms of the GNU General Public License as published by
 * # the Free Software Foundation, either version 3 of the License, or
 * # (at your option) any later version.
 * #
 * # UIC ControlDigit  is distributed in the hope that it will be useful,
 * # but WITHOUT ANY WARRANTY; without even the implied warranty of
 * # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * # GNU General Public License for more details.
 * #
 * # You should have received a copy of the GNU General Public License
 * # along with UIC ControlDigit . If not, see <http://www.gnu.org/licenses/>.
 */

package com.steffendev.manfred.dbkontrollziff;

class ControlDigitUIC {

    private static final String LOG_TAG = ControlDigitUIC.class.getSimpleName();

    private String number;

    ControlDigitUIC(String number) {
        this.number = number;
    }

    int getControlDigitUICWagon() {
        //Write each digit in separate Arrayindex
        int[] DigitArrayNumber = new int[11];
        for (int i = 0; i < number.length(); i++) {
            DigitArrayNumber[i] = Character.digit(number.charAt(i), 10);
        }
        //Multiplicate each digit with the multiplication factors
        int[] multiplicationResult = new int[22];
        int[] multiplicationFactors = new int[]{2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2};
        int x = 0;
        for (int i = 0; i < DigitArrayNumber.length; i++) {
            if ((DigitArrayNumber[i] * multiplicationFactors[i]) <= 9) {
                multiplicationResult[x] = DigitArrayNumber[i] * multiplicationFactors[i];
            } else {
                multiplicationResult[x] = 1;
                x++;
                multiplicationResult[x] = (DigitArrayNumber[i] * multiplicationFactors[i]) - 10;
            }
            x++;
        }
        //Calculate cross sum over the digits stored in multiplication result
        double crossSum = 0;
        for (int aMultiplicationResult : multiplicationResult) {
            crossSum = crossSum + aMultiplicationResult;
        }
        double Modulo = Math.ceil(crossSum / 10) * 10;
        return (int) Modulo - (int) crossSum;
    }

}
