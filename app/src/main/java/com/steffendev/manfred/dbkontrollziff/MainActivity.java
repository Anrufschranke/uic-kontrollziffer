/*
 * # Copyright (C) 2019 Manfred Steffen
 * # This file is part of UIC ControlDigit <https://gitlab.com/Anrufschranke/uic-kontrollziffer>.
 * #
 * # UIC ControlDigit  is free software: you can redistribute it and/or modify
 * # it under the terms of the GNU General Public License as published by
 * # the Free Software Foundation, either version 3 of the License, or
 * # (at your option) any later version.
 * #
 * # UIC ControlDigit  is distributed in the hope that it will be useful,
 * # but WITHOUT ANY WARRANTY; without even the implied warranty of
 * # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * # GNU General Public License for more details.
 * #
 * # You should have received a copy of the GNU General Public License
 * # along with UIC ControlDigit . If not, see <http://www.gnu.org/licenses/>.
 */

package com.steffendev.manfred.dbkontrollziff;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        activateListener();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cab_info:
                Intent intentActivityInfo = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intentActivityInfo);
                break;
        }
        return true;
    }

    private void activateListener() {
        final EditText editTextNumberDB1 = findViewById(R.id.editTextDBNumber1);
        final EditText editTextNumberDB2 = findViewById(R.id.editTextDBNumber2);
        final EditText editTextNumberUICWagon = findViewById(R.id.editTextUICWagon);
        final EditText editTextNumberOEBB1 = findViewById(R.id.editTextOEBBNumber1);
        final EditText editTextNumberOEBB2 = findViewById(R.id.editTextOEBBNumber2);
        final TextView textViewControlDigitDB = findViewById(R.id.textViewDBControlDigit);
        final TextView textViewControlDigitOEBB = findViewById(R.id.textViewOEBBControlDigit);
        final TextView textViewControlDigitUICWagon = findViewById(R.id.textViewUICWagonControlDigit);


        editTextNumberDB1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 3) {
                    editTextNumberDB2.requestFocus();
                }
                if (s.length() != 3) {
                    textViewControlDigitDB.setText(getResources().getString(R.string.control_digit_default));
                }
                if (editTextNumberDB1.getText().length() == 3 && editTextNumberDB2.getText().length() == 3 && s.length() == 3) {
                    ControlDigitDB controlDigitDB = new ControlDigitDB(editTextNumberDB1.getText().toString(), editTextNumberDB2.getText().toString());
                    textViewControlDigitDB.setText(String.valueOf(controlDigitDB.getControlDigitDB()));
                }
            }
        });

        editTextNumberDB2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    editTextNumberDB1.requestFocus();
                }
                if (s.length() != 3) {
                    textViewControlDigitDB.setText(getResources().getString(R.string.control_digit_default));
                }
                if (editTextNumberDB1.getText().length() == 3 && editTextNumberDB2.getText().length() == 3 && s.length() == 3) {
                    ControlDigitDB controlDigitDB = new ControlDigitDB(editTextNumberDB1.getText().toString(), editTextNumberDB2.getText().toString());
                    textViewControlDigitDB.setText(String.valueOf(controlDigitDB.getControlDigitDB()));
                }
            }
        });

        editTextNumberOEBB1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 4) {
                    editTextNumberOEBB2.requestFocus();
                }
                if (s.length() != 4) {
                    textViewControlDigitOEBB.setText(getResources().getString(R.string.control_digit_default));
                }
                if (editTextNumberOEBB1.getText().length() == 4 && editTextNumberOEBB2.length() == 3 && s.length() == 4) {
                    ControlDigitOEBB controlDigitOEBB = new ControlDigitOEBB(editTextNumberOEBB1.getText().toString(), editTextNumberOEBB2.getText().toString());
                    textViewControlDigitOEBB.setText(String.valueOf(controlDigitOEBB.getControlDigitOEBB()));
                }
            }
        });

        editTextNumberOEBB2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    editTextNumberOEBB1.requestFocus();
                }
                if (s.length() != 3) {
                    textViewControlDigitOEBB.setText(getResources().getString(R.string.control_digit_default));
                }
                if (editTextNumberOEBB1.getText().length() == 4 && editTextNumberOEBB2.getText().length() == 3 && s.length() == 3) {
                    ControlDigitOEBB controlDigitOEBB = new ControlDigitOEBB(editTextNumberOEBB1.getText().toString(), editTextNumberOEBB2.getText().toString());
                    textViewControlDigitOEBB.setText(String.valueOf(controlDigitOEBB.getControlDigitOEBB()));
                }
            }
        });

        editTextNumberUICWagon.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 11) {
                    ControlDigitUIC controlDigitUIC = new ControlDigitUIC(s.toString());
                    textViewControlDigitUICWagon.setText(String.valueOf(controlDigitUIC.getControlDigitUICWagon()));
                }
                if (s.length() != 11) {
                    textViewControlDigitUICWagon.setText(getResources().getString(R.string.control_digit_default));
                }
            }
        });

    }

}
